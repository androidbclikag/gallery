package com.example.gallery.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.example.gallery.R
import com.example.gallery.models.ImageModel
import kotlinx.android.synthetic.main.item_layout.view.*

class ViewPagerAdapterForLayout(private var items: ArrayList<ImageModel>) : PagerAdapter() {
    override fun isViewFromObject(view: View, itemView: Any): Boolean {
        return view == itemView
    }

    override fun getCount(): Int = items.size
    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val itemView =
            LayoutInflater.from(container.context).inflate(R.layout.item_layout, container, false)
        val pos = "image position in the array:$position"
        val item = items[position]
        Glide.with(container.context).load(item.imageUrl).placeholder(R.mipmap.ic_launcher_round)
            .into(itemView!!.imageView)
        itemView.textView.text = pos


        container.addView(itemView)
        return itemView
    }

    override fun destroyItem(container: ViewGroup, position: Int, itemView: Any) {
        container.removeView(itemView as View)
    }


}