package com.example.gallery.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.example.gallery.fragments.ImageGalleryFragment
import com.example.gallery.models.ImageModel

class ViewPagerAdapter(fm:FragmentManager, behavior:Int, private val items:ArrayList<ImageModel>):FragmentStatePagerAdapter(fm, behavior){
    override fun getItem(position: Int): Fragment  = ImageGalleryFragment(items[position], position)

    override fun getCount(): Int = items.size


}