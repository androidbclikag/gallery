package com.example.gallery.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.example.gallery.R
import com.example.gallery.models.ImageModel
import kotlinx.android.synthetic.main.fragment_gallery.view.*

class ImageGalleryFragment(var image:ImageModel, var position: Int): BaseFragment() {
    override fun start(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) {
        init()
    }


    override fun getLayoutResource(): Int  = R.layout.fragment_gallery

    private fun init(){
        val pos = "image position in the array:$position"
        Glide.with(this).load(image.imageUrl).placeholder(R.mipmap.ic_launcher_round).into(rootView!!.imageView)
        rootView!!.textView.text = pos
    }
}