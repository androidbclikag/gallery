package com.example.gallery.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.viewpager.widget.ViewPager
import com.example.gallery.R
import com.example.gallery.adapter.ViewPagerAdapterForLayout
import com.example.gallery.fragments.ImageGalleryFragment
import com.example.gallery.models.ImageModel
import kotlinx.android.synthetic.main.activity_layout_gallery.*

class LayoutGalleryActivity : AppCompatActivity() {
    private lateinit var mPager: ViewPager

    private val items = ArrayList<ImageModel>()
    private lateinit var adapter: ViewPagerAdapterForLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_layout_gallery)
        init()

        goToFragmentButton.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)

        }
    }


    private fun init() {
        mPager = viewPagerLayouts
        adapter = ViewPagerAdapterForLayout(items)
        mPager.adapter = adapter
        setResources()
        adapter.notifyDataSetChanged()


    }

    private fun setResources() {

        val albumOne = "https://upload.wikimedia.org/wikipedia/en/0/03/Queen_Queen.png"
        val albumTwo =
            "https://upload.wikimedia.org/wikipedia/en/thumb/4/4d/Queen_A_Night_At_The_Opera.png/220px-Queen_A_Night_At_The_Opera.png"
        val albumThree =
            "https://upload.wikimedia.org/wikipedia/en/thumb/f/f7/Queen_Innuendo.png/220px-Queen_Innuendo.png"
        val albumFour =
            "https://upload.wikimedia.org/wikipedia/ka/thumb/a/ad/Queen_II.jpg/230px-Queen_II.jpg"
        val albumFive =
            "https://upload.wikimedia.org/wikipedia/en/thumb/0/06/Queen_Jazz.png/220px-Queen_Jazz.png"

        val imageModel0 = ImageModel(albumOne)
        val imageModel1 = ImageModel(albumTwo)
        val imageModel2 = ImageModel(albumThree)
        val imageModel3 = ImageModel(albumFour)
        val imageModel4 = ImageModel(albumFive)

        items.add(imageModel0)
        items.add(imageModel1)
        items.add(imageModel2)
        items.add(imageModel3)
        items.add(imageModel4)
    }
}
